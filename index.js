// console.log('Hello, World');

// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Chito",
    "lastName": "Merienda",
    "email": "cmerienda@mail.com",
    "password": "bagsakanin543",
    "isAdmin": false,
    "mobileNo": "09333333333"
}


// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 99,
    "userId": 2,
    "productID" : 9999,
    "transactionDate": "12-21-2068",
    "status": "paid",
    "total": 9999
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true
}

{
    "id": 9999,
    "name": "Air Conditioner",
    "description": "Cools/Heats up your room temperature.",
    "price": 1,
    "stocks": 9999,
    "isActive": true
}
